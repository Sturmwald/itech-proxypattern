﻿namespace ProxyPattern
{
    interface ISeed
    {
        string Seed { get;}
        int As { get; }
        int Bs { get; }
        int Cs { get; }
        int Ds { get; }
        int Es { get; }
        int Fs { get; }
        int Gs { get; }
    }
}
