﻿using System;
using System.Linq;

namespace ProxyPattern
{
    class SeedGenerator : ISeed
    {
        public string Seed { get; private set; }
        public int As { get; private set; }
        public int Bs { get; private set; }
        public int Cs { get; private set; }
        public int Ds { get; private set; }
        public int Es { get; private set; }
        public int Fs { get; private set; }
        public int Gs { get; private set; }

        public SeedGenerator(int length)
        {

            Seed = RandomString(length);
            As = Seed.Count(f => f == 'A');
            Bs = Seed.Count(f => f == 'B');
            Cs = Seed.Count(f => f == 'C');
            Ds = Seed.Count(f => f == 'D');
            Es = Seed.Count(f => f == 'E');
            Fs = Seed.Count(f => f == 'F');
            Gs = Seed.Count(f => f == 'G');

        }

        private string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
