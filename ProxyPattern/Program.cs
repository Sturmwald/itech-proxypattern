﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProxyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var timer = new Stopwatch();
            timer.Start();
            var Set = new List<ISeed>();
            for (int i = 0; i < 15; i++)
            {
                Set.Add(new SeedGenerator(5000000));
            }
            var temp = Set[0].Seed;
            Console.WriteLine(Set[2].As.ToString(), Environment.NewLine);
            Console.WriteLine(Set[2].Bs.ToString(), Environment.NewLine);
            Console.WriteLine(Set[6].Cs.ToString(), Environment.NewLine);
            Console.WriteLine(Set[8].Ds.ToString(), Environment.NewLine);
            timer.Stop();
            Console.WriteLine("Total time elapsed: " + timer.Elapsed);
            Console.WriteLine("Presss enter to compare with proxy");
            Console.ReadLine();
            var timerProxy = new Stopwatch();
            timerProxy.Start();
            var SetProxy = new List<ISeed>();
            for (int i = 0; i < 15; i++)
            {
                SetProxy.Add(new Proxy(5000000));
            }
            var tempP = SetProxy[0].Seed;
            Console.WriteLine(SetProxy[2].As.ToString(), Environment.NewLine);
            Console.WriteLine(SetProxy[2].Bs.ToString(), Environment.NewLine);
            Console.WriteLine(SetProxy[6].Cs.ToString(), Environment.NewLine);
            Console.WriteLine(SetProxy[8].Ds.ToString(), Environment.NewLine);
            timerProxy.Stop();

            Console.WriteLine("Total time elapsed for Proxy: " + timerProxy.Elapsed);
            Console.WriteLine("Total time elapsed without  : " + timer.Elapsed);
            Console.ReadLine();

        }
    }
}
