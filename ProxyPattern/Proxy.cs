﻿namespace ProxyPattern
{
    class Proxy : ISeed
    {
        private SeedGenerator seed;
        private int length;

        public Proxy(int count)
        {
            length = count;
        }

        public string Seed
        {
            get
            {
                if (seed != null)
                {
                    return seed.Seed;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Seed;
                }
            }
        }

        public int As
        {
            get
            {
                if (seed != null)
                {
                    return seed.As;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.As;
                }
            }
        }
        public int Bs
        {
            get
            {
                if (seed != null)
                {
                    return seed.Bs;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Bs;
                }
            }
        }
        public int Cs
        {
            get
            {
                if (seed != null)
                {
                    return seed.Cs;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Cs;
                }
            }
        }
        public int Ds
        {
            get
            {
                if (seed != null)
                {
                    return seed.Ds;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Ds;
                }
            }
        }
        public int Es
        {
            get
            {
                if (seed != null)
                {
                    return seed.Es;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Es;
                }
            }
        }
        public int Fs
        {
            get
            {
                if (seed != null)
                {
                    return seed.Fs;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Fs;
                }
            }
        }
        public int Gs
        {
            get
            {
                if (seed != null)
                {
                    return seed.Gs;
                }
                else
                {
                    seed = new SeedGenerator(length);
                    return seed.Gs;
                }
            }
        }
    }
}
